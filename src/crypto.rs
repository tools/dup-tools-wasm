//  Copyright (C) 2020  Éloïs SANCHEZ.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::*;

#[wasm_bindgen]
pub fn verify(datas: &str, pubkey: &str, signature: &str) -> bool {
    let pubkey = match ed25519::PublicKey::from_base58(pubkey) {
        Ok(pubkey) => pubkey,
        Err(_) => return false,
    };
    let sig = match ed25519::Signature::from_base64(signature) {
        Ok(signature) => signature,
        Err(_) => return false,
    };

    pubkey.verify(datas.as_bytes(), &sig).is_ok()
}

#[wasm_bindgen]
pub fn generate_random_seed() -> String {
    match Seed32::random() {
        Ok(seed) => seed.to_base58(),
        Err(_) => "fail to generate random seed".to_owned(),
    }
}

#[wasm_bindgen]
pub fn generate_ed25519_pubkey(salt: String, password: String) -> String {
    let keypair = ed25519::KeyPairFromSaltedPasswordGenerator::with_default_parameters()
        .generate(ed25519::SaltedPassword::new(salt, password));

    format!("{}", keypair.public_key())
}

#[wasm_bindgen]
pub fn generate_seed_from_salted_password(salt: String, password: String) -> String {
    let seed = ed25519::KeyPairFromSaltedPasswordGenerator::with_default_parameters()
        .generate_seed(password.as_bytes(), salt.as_bytes());

    seed.to_base58()
}

#[wasm_bindgen]
pub fn generate_ed25519_pubkey_from_seed(seed: &str, base: usize) -> String {
    match parse_seed(seed, base) {
        Ok(seed_bytes) => {
            let keypair = ed25519::KeyPairFromSeed32Generator::generate(Seed32::new(seed_bytes));
            format!("{}", keypair.public_key())
        }
        Err(error_message) => error_message,
    }
}

fn parse_seed(seed: &str, base: usize) -> Result<[u8; 32], String> {
    match base {
        16 => match b16::str_hex_to_32bytes(seed) {
            Ok(seed) => Ok(seed),
            Err(err) => Err(format!("{}", err)),
        },
        58 => match b58::str_base58_to_32bytes(seed) {
            Ok((seed, _leading_1)) => Ok(seed),
            Err(err) => Err(format!("{}", err)),
        },
        _ => Err("Error : invalid param base : accepted values are : 16, 58.".to_owned()),
    }
}
