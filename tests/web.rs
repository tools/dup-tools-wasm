//! Test suite for the Web and headless browsers.

#![cfg(target_arch = "wasm32")]

extern crate dup_tools_wasm;
extern crate wasm_bindgen_test;

use dup_tools_wasm::crypto::*;
use dup_tools_wasm::examples::*;
use dup_tools_wasm::parsers::*;
use dup_tools_wasm::*;
//use pretty_assertions::assert_eq;
use wasm_bindgen_test::*;

wasm_bindgen_test_configure!(run_in_browser);

#[wasm_bindgen_test]
fn test_generate_random_seed() {
    let seed1 = generate_random_seed();
    let seed2 = generate_random_seed();

    assert_ne!(seed1, seed2);
}

#[wasm_bindgen_test]
fn test_generate_ed25519_keypair() {
    let pubkey = generate_ed25519_pubkey(
        "JhxtHB7UcsDbA9wMSyMKXUzBZUQvqVyB32KwzS9SWoLkjrUhHV".to_owned(),
        "JhxtHB7UcsDbA9wMSyMKXUzBZUQvqVyB32KwzS9SWoLkjrUhHV_".to_owned(),
    );

    assert_eq!(
        pubkey.as_str(),
        "7iMV3b6j2hSj5WtrfchfvxivS9swN3opDgxudeHq64fb"
    );
}

#[wasm_bindgen_test]
fn test_generate_seed_from_salted_password() {
    let seed_b58 = generate_seed_from_salted_password(
        "JhxtHB7UcsDbA9wMSyMKXUzBZUQvqVyB32KwzS9SWoLkjrUhHV".to_owned(),
        "JhxtHB7UcsDbA9wMSyMKXUzBZUQvqVyB32KwzS9SWoLkjrUhHV_".to_owned(),
    );

    assert_eq!(seed_b58, "AhCNguKBPxaG6yZAcX53c12kybubCXn3fjbR5LnCkrQu");
}

#[wasm_bindgen_test]
fn test_generate_ed25519_keypair_from_seed() {
    let seed_b58 = "AhCNguKBPxaG6yZAcX53c12kybubCXn3fjbR5LnCkrQu";

    let pubkey_from_seed_b58 = generate_ed25519_pubkey_from_seed(seed_b58, 58);

    assert_eq!(
        pubkey_from_seed_b58.as_str(),
        "7iMV3b6j2hSj5WtrfchfvxivS9swN3opDgxudeHq64fb"
    );
}

#[wasm_bindgen_test]
fn test_parse_and_verify_example_idty_v10() {
    let doc = example_doc(DocumentType::IdentityV10);

    assert_eq!(
        parse_doc_into_json(&doc, DocumentType::Any),
        "{\n  \"Identity\": {\n    \"V10\": {\n      \"currency\": \"duniter_unit_test_currency\",\n      \"username\": \"tic\",\n      \"blockstamp\": \"0-E3B0C44298FC1C149AFBF4C8996FB92427AE41E4649B934CA495991B7852B855\",\n      \"issuer\": \"DNann1Lh55eZMEDXeYt59bzHbA3NJR46DeQYCS2qQdLV\",\n      \"signature\": \"1eubHHbuNfilHMM0G2bI30iZzebQ2cQ1PC7uPAw08FGMMmQCRerlF/3pc4sAcsnexsxBseA/3lY03KlONqJBAg==\"\n    }\n  }\n}".to_owned()
    );
    assert_eq!(
        parse_doc_and_verify(&doc, DocumentType::Any),
        DocCheckResult::ValidSig,
    );
}

#[wasm_bindgen_test]
fn test_parse_and_verify_example_membership_v10() {
    let doc = example_doc(DocumentType::MembershipV10);

    assert_eq!(
        parse_doc_into_json(&doc, DocumentType::Any),
        "{\n  \"Membership\": {\n    \"V10\": {\n      \"currency\": \"duniter_unit_test_currency\",\n      \"issuer\": \"DNann1Lh55eZMEDXeYt59bzHbA3NJR46DeQYCS2qQdLV\",\n      \"blockstamp\": \"0-E3B0C44298FC1C149AFBF4C8996FB92427AE41E4649B934CA495991B7852B855\",\n      \"membership\": \"IN\",\n      \"username\": \"tic\",\n      \"identity_blockstamp\": \"0-E3B0C44298FC1C149AFBF4C8996FB92427AE41E4649B934CA495991B7852B855\",\n      \"signature\": \"s2hUbokkibTAWGEwErw6hyXSWlWFQ2UWs2PWx8d/kkElAyuuWaQq4Tsonuweh1xn4AC1TVWt4yMR3WrDdkhnAw==\"\n    }\n  }\n}".to_owned()
    );
    assert_eq!(
        parse_doc_and_verify(&doc, DocumentType::Any),
        DocCheckResult::ValidSig,
    );
}

#[wasm_bindgen_test]
fn test_parse_and_verify_example_cert_v10() {
    let doc = example_doc(DocumentType::CertificationV10);

    assert_eq!(
        parse_doc_into_json(&doc, DocumentType::Any),
        "{\n  \"Certification\": {\n    \"V10\": {\n      \"currency\": \"g1-test\",\n      \"issuer\": \"5B8iMAzq1dNmFe3ZxFTBQkqhq4fsztg1gZvxHXCk1XYH\",\n      \"target\": \"mMPioknj2MQCX9KyKykdw8qMRxYR2w1u3UpdiEJHgXg\",\n      \"identity_username\": \"mmpio\",\n      \"identity_blockstamp\": \"7543-000044410C5370DE8DBA911A358F318096B7A269CFC2BB93272E397CC513EA0A\",\n      \"identity_sig\": \"SmSweUD4lEMwiZfY8ux9maBjrQQDkC85oMNsin6oSQCPdXG8sFCZ4FisUaWqKsfOlZVb/HNa+TKzD2t0Yte+DA==\",\n      \"blockstamp\": \"167884-0001DFCA28002A8C96575E53B8CEF8317453A7B0BA255542CCF0EC8AB5E99038\",\n      \"signature\": \"wqZxPEGxLrHGv8VdEIfUGvUcf+tDdNTMXjLzVRCQ4UhlhDRahOMjfcbP7byNYr5OfIl83S1MBxF7VJgu8YasCA==\"\n    }\n  }\n}".to_owned()
    );
    assert_eq!(
        parse_doc_and_verify(&doc, DocumentType::Any),
        DocCheckResult::ValidSig,
    );
}

#[wasm_bindgen_test]
fn test_parse_and_verify_example_revoc_v10() {
    let doc = example_doc(DocumentType::RevocationV10);

    assert_eq!(
        parse_doc_into_json(&doc, DocumentType::Any),
        "{\n  \"Revocation\": {\n    \"V10\": {\n      \"currency\": \"g1\",\n      \"issuer\": \"DNann1Lh55eZMEDXeYt59bzHbA3NJR46DeQYCS2qQdLV\",\n      \"identity_username\": \"tic\",\n      \"identity_blockstamp\": \"0-E3B0C44298FC1C149AFBF4C8996FB92427AE41E4649B934CA495991B7852B855\",\n      \"identity_sig\": \"1eubHHbuNfilHMM0G2bI30iZzebQ2cQ1PC7uPAw08FGMMmQCRerlF/3pc4sAcsnexsxBseA/3lY03KlONqJBAg==\",\n      \"signature\": \"XXOgI++6qpY9O31ml/FcfbXCE6aixIrgkT5jL7kBle3YOMr+8wrp7Rt+z9hDVjrNfYX2gpeJsuMNfG4T/fzVDQ==\"\n    }\n  }\n}".to_owned()
    );
    assert_eq!(
        parse_doc_and_verify(&doc, DocumentType::Any),
        DocCheckResult::ValidSig,
    );
}

#[wasm_bindgen_test]
fn test_parse_and_verify_example_tx_v10() {
    let doc = example_doc(DocumentType::TxV10);

    assert_eq!(
        parse_doc_into_json(&doc, DocumentType::Any),
        "{\n  \"Transaction\": {\n    \"V10\": {\n      \"currency\": \"g1\",\n      \"blockstamp\": \"107702-0000017CDBE974DC9A46B89EE7DC2BEE4017C43A005359E0853026C21FB6A084\",\n      \"locktime\": 0,\n      \"issuers\": [\n        \"Do6Y6nQ2KTo5fB4MXbSwabXVmXHxYRB9UUAaTPKn1XqC\"\n      ],\n      \"inputs\": [\n        \"1002:0:D:Do6Y6nQ2KTo5fB4MXbSwabXVmXHxYRB9UUAaTPKn1XqC:104937\",\n        \"1002:0:D:Do6Y6nQ2KTo5fB4MXbSwabXVmXHxYRB9UUAaTPKn1XqC:105214\"\n      ],\n      \"unlocks\": [\n        \"0:SIG(0)\",\n        \"1:SIG(0)\"\n      ],\n      \"outputs\": [\n        \"2004:0:SIG(DTgQ97AuJ8UgVXcxmNtULAs8Fg1kKC1Wr9SAS96Br9NG)\"\n      ],\n      \"comment\": \"c est pour 2 mois d adhesion ressourcerie\",\n      \"signatures\": [\n        \"lnpuFsIymgz7qhKF/GsZ3n3W8ZauAAfWmT4W0iJQBLKJK2GFkesLWeMj/+GBfjD6kdkjreg9M6VfkwIZH+hCCQ==\"\n      ],\n      \"hash\": null\n    }\n  }\n}".to_owned()
    );
    assert_eq!(
        parse_doc_and_verify(&doc, DocumentType::Any),
        DocCheckResult::ValidSig,
    );
}

/*#[wasm_bindgen_test]
fn test_parse_and_verify_example_peer_v11() {
    let doc = example_doc(DocumentType::PeerV11);

    assert_eq!(
        parse_doc_into_json(&doc, DocumentType::Any),
        "{\n  \"currency_name\": \"g1\",\n  \"issuer\": \"7iMV3b6j2hSj5WtrfchfvxivS9swN3opDgxudeHq64fb\",\n  \"node_id\": \"0\",\n  \"blockstamp\": \"50-000005B1CEB4EC5245EF7E33101A330A1C9A358EC45A25FC13F78BB58C9E7370\",\n  \"endpoints\": [\n    \"WS2P V2 S 7 g1.durs.ifee.fr 443 ws2p\",\n    \"WS2P V2 S 7 84.16.72.210 443 ws2p\"\n  ],\n  \"sig\": \"EQ2D5almq2RNUi3XZNtTpjo9nWtJF0PzsCW7ROAzCQKiEtpI7/fW8Z23GJ2a/SIxfYSzlq/cZqksE4EoVe1rAw==\"\n}".to_owned()
    );
    assert_eq!(
        parse_doc_and_verify(&doc, DocumentType::Any),
        DocCheckResult::ValidSig,
    );
}*/

/*#[wasm_bindgen_test]
fn test_parse_and_verify_example_head_v3() {
    let doc = example_doc(DocumentType::HeadV3);

    assert_eq!(
        parse_doc_into_json(&doc, DocumentType::Any),
        "{\n  \"body\": \"3:g1:0:0:0:0:0:7iMV3b6j2hSj5WtrfchfvxivS9swN3opDgxudeHq64fb:50-000005B1CEB4EC5245EF7E33101A330A1C9A358EC45A25FC13F78BB58C9E7370:durs:0.1.0-a0.1\",\n  \"signature\": \"vwlxpkCbv83qYSiClYA/GD35hs0AsZBnqv7uoE8hqlarT2c6jVRKhjp8JBqmRI7Se4IDwC2owk0mF4CglvyACQ==\",\n  \"step\": 2\n}".to_owned()
    );
    assert_eq!(
        parse_doc_and_verify(&doc, DocumentType::Any),
        DocCheckResult::ValidSig,
    );
}*/

#[wasm_bindgen_test]
fn test_verify_sig() {
    assert!(verify(
        "WS2POCAIC:HEAD:1:4C4jsvxmFQBoHN86BHsSreTKoK2bYvQV9gAGtH3BMNc3:167482-000000309D633082BCF18046F1291D28EA8912C85B4BEA437E46D1EF342E9E1F:26856cc8:duniter:1.6.25:1",
        "4C4jsvxmFQBoHN86BHsSreTKoK2bYvQV9gAGtH3BMNc3",
        "WDgS+ZtHgMLjcBOVCRAelldrfYpJka64A8ICIwQW4dX/AR/bNrMfbj3hSZSzF+27ClJ4Z6wH1jDwMHHGOiNsCQ=="
    ));
}
