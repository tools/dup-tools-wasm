//  Copyright (C) 2020  Éloïs SANCHEZ.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

pub mod crypto;
pub mod examples;
pub mod parsers;

// Crate imports
pub(crate) use cfg_if::cfg_if;
pub(crate) use dubp_documents::certification::*;
pub(crate) use dubp_documents::dubp_common::crypto::bases::b58::ToBase58;
pub(crate) use dubp_documents::dubp_common::crypto::bases::*;
pub(crate) use dubp_documents::dubp_common::crypto::keys::*;
pub(crate) use dubp_documents::identity::*;
pub(crate) use dubp_documents::membership::*;
pub(crate) use dubp_documents::prelude::*;
pub(crate) use dubp_documents::revocation::*;
pub(crate) use dubp_documents::transaction::*;
pub(crate) use dubp_documents_parser::prelude::*;
pub(crate) use dubp_documents_parser::{PestError, RawTextParseError};
pub(crate) use std::ops::Deref;
pub(crate) use wasm_bindgen::prelude::*;

cfg_if! {
    // When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
    // allocator.
    if #[cfg(feature = "wee_alloc")] {
        extern crate wee_alloc;
        #[global_allocator]
        static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;
    }
}
cfg_if! {
    // When the `console_error_panic_hook` feature is enabled, we can call the
    // `set_panic_hook` function at least once during initialization, and then
    // we will get better error messages if our code ever panics.
    //
    // For more details see
    // https://github.com/rustwasm/console_error_panic_hook#readme
    if #[cfg(feature = "console_error_panic_hook")] {
        extern crate console_error_panic_hook;
        pub use self::console_error_panic_hook::set_once as set_panic_hook;
    } else {
        #[inline]
        pub fn set_panic_hook() {}
    }
}

#[wasm_bindgen]
#[derive(Debug, Copy, Clone)]
pub enum DocumentType {
    Any,
    TxV10,
    IdentityV10,
    MembershipV10,
    CertificationV10,
    RevocationV10,
    //PeerV11,
    //HeadV3,
}
