//  Copyright (C) 2020  Éloïs SANCHEZ.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

pub mod wallet_script;

use crate::*;
//use durs_network_documents::network_head::NetworkHead;
//use durs_network_documents::network_head_v3::NetworkHeadV3;
//use durs_network_documents::network_peer::*;
//use durs_network_documents::*;

#[wasm_bindgen]
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum DocCheckResult {
    UnknownType,
    WrongFormat,
    InvalidSig,
    ValidSig,
}

#[derive(Debug, Clone)]
pub enum Document_ {
    DubpDocument(DubpDocument),
    //NetworkDocument(NetworkDocument),
}

#[wasm_bindgen]
pub fn parse_doc_and_verify(source: &str, doc_type: DocumentType) -> DocCheckResult {
    if let Ok(document) = parse_doc(source, doc_type) {
        let valid_sig = match document {
            Document_::DubpDocument(blockchain_document) => match blockchain_document {
                DubpDocument::Transaction(doc) => doc.verify_signatures().is_ok(),
                DubpDocument::Identity(doc) => doc.verify_signatures().is_ok(),
                DubpDocument::Membership(doc) => doc.verify_signatures().is_ok(),
                DubpDocument::Certification(doc) => doc.verify_signatures().is_ok(),
                DubpDocument::Revocation(doc) => doc.verify_signatures().is_ok(),
            },
            /*Document_::NetworkDocument(network_document) => match network_document {
                NetworkDocument::Peer(peer_box) => match peer_box.deref() {
                    PeerCard::V11(peer_v11) => peer_v11.verify().is_ok(),
                    _ => unimplemented!(),
                },
                NetworkDocument::Head(head) => match head {
                    NetworkHead::V3(head_v3_box) => head_v3_box.deref().verify().is_ok(),
                    _ => unimplemented!(),
                },
            },*/
        };
        if valid_sig {
            DocCheckResult::ValidSig
        } else {
            DocCheckResult::InvalidSig
        }
    } else if let DocumentType::Any = doc_type {
        DocCheckResult::UnknownType
    } else {
        DocCheckResult::WrongFormat
    }
}

#[wasm_bindgen]
pub fn parse_doc_into_json(doc: &str, doc_type: DocumentType) -> String {
    match parse_doc(doc, doc_type) {
        Ok(document) => match document {
            Document_::DubpDocument(blockchain_document) => match blockchain_document {
                DubpDocument::Transaction(ref tx_box) => {
                    let tx = tx_box.deref().clone();
                    tx.compute_hash();
                    DubpDocument::to_json_string_pretty(&DubpDocument::Transaction(tx))
                        .unwrap_or_else(|_| "Fail to jsonifie document.".to_owned())
                }
                _ => DubpDocument::to_json_string_pretty(&blockchain_document)
                    .unwrap_or_else(|_| "Fail to jsonifie document.".to_owned()),
            },
            /*Document_::NetworkDocument(network_document) => match network_document {
                NetworkDocument::Peer(peer_box) => match peer_box.deref() {
                    PeerCard::V11(peer_v11) => PeerCardV11::to_json_string_pretty(&peer_v11)
                        .unwrap_or_else(|_| "Fail to jsonifie document.".to_owned()),
                    _ => unimplemented!(),
                },
                NetworkDocument::Head(head) => match head {
                    NetworkHead::V3(head_v3) => NetworkHeadV3::to_json_string_pretty(&head_v3)
                        .unwrap_or_else(|_| "Fail to jsonifie document.".to_owned()),
                    _ => unimplemented!(),
                },
            },*/
        },
        Err(parse_error) => match parse_error {
            RawTextParseError::UnknownType => {
                "Invalid document. Specify the expected document type for more information."
                    .to_owned()
            }
            RawTextParseError::PestError(PestError(pest_error)) => pest_error,
            _ => format!("{:?}", parse_error),
        },
    }
}

fn parse_doc(doc: &str, doc_type: DocumentType) -> Result<Document_, RawTextParseError> {
    match doc_type {
        DocumentType::Any => /*match NetworkDocument::parse(doc) {
            Ok(network_document) => Ok(Document_::NetworkDocument(network_document)),
            Err(_) => match DubpDocument::parse(doc) {
                Ok(blockchain_document) => Ok(Document_::DubpDocument(blockchain_document)),
                Err(_) => Err(RawTextParseError::UnknownType),
            },*/
            match DubpDocument::parse_from_raw_text(doc) {
                Ok(blockchain_document) => Ok(Document_::DubpDocument(blockchain_document)),
                Err(_) => Err(RawTextParseError::UnknownType),
            }
        //},
        DocumentType::TxV10 => Ok(Document_::DubpDocument(DubpDocument::Transaction(
            TransactionDocument::parse_from_raw_text(doc)?,
        ))),
        DocumentType::IdentityV10 => Ok(Document_::DubpDocument(DubpDocument::Identity(
            IdentityDocument::parse_from_raw_text(doc)?,
        ))),
        DocumentType::MembershipV10 => Ok(Document_::DubpDocument(DubpDocument::Membership(
            MembershipDocument::parse_from_raw_text(doc)?,
        ))),
        DocumentType::CertificationV10 => Ok(Document_::DubpDocument(DubpDocument::Certification(
            CertificationDocument::parse_from_raw_text(doc)?,
        ))),
        DocumentType::RevocationV10 => Ok(Document_::DubpDocument(DubpDocument::Revocation(
            RevocationDocument::parse_from_raw_text(doc)?,
        ))),
        /*DocumentType::PeerV11 => Ok(Document_::NetworkDocument(NetworkDocument::Peer(Box::new(
            PeerCard::V11(PeerCardV11::parse(doc)?),
        )))),
        DocumentType::HeadV3 => Ok(Document_::NetworkDocument(NetworkDocument::Head(
            NetworkHead::V3(Box::new(NetworkHeadV3::parse(doc)?)),
        ))),*/
    }
}
