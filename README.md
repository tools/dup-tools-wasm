<meta charset="utf-8"/>

# DUP Tools

A tools box lib for DUP (Dividend Universal Protocol) in WebAssembly (WASM).

- Rust **reliability**
- WebAssembly **performances**
- JavaScript **wide usability**

You can use DUP tools directly in [modern browsers](https://caniuse.com/#feat=wasm) or server side with [node.js](https://nodejs.org/).

## Project state

[![npm version](https://img.shields.io/npm/v/dup-tools-wasm.svg)](https://www.npmjs.com/package/dup-tools-wasm)
[![pipeline status](https://git.duniter.org/tools/dup-tools-wasm/badges/master/pipeline.svg)](https://git.duniter.org/tools/dup-tools-wasm/commits/master)
[![coverage report](https://git.duniter.org/tools/dup-tools-wasm/badges/master/coverage.svg)](https://git.duniter.org/tools/dup-tools-wasm/commits/master)

## 📝 Usage

[Full usage example with online demo](https://git.duniter.org/tools/dup-tools-front#readme)

### ⚡  Quick start guide

1. install [npm](https://www.npmjs.com/get-npm)
2. Create an empty folder for your project, and open a terminal in it.
3. To install `dup-tools-wasm` run

    ```bash
    npm install dup-tools-wasm
    ```

4. create a js file `myFirstExperiment.js` with the following code :

    ```javascript
    import * as dup from "dup-tools-wasm";

    const keypair = dup.generate_ed25519_keypair("salt", "password");
    console.log(keypair);
    ```

    >>> WiP

5. ![Fork](http://forked.yannick.io/images/favicon.png) for in browser use case :

    create an html file `index.html` with the following code :

    ```html
    <meta charset="utf-8"/>
    <h1>See your console result (F12 by default)</h1>
    <script module src="myFirstExperiment.js"></script>
    ```

6. ![Fork](http://forked.yannick.io/images/favicon.png) for node.js use case :

>>> End WiP

## Contribute

### 🔮 Prerequisites

You need [wasm-pack](https://github.com/rustwasm/wasm-pack#--wasm-pack) and it prerequisites ([Rust and npm](https://rustwasm.github.io/wasm-pack/book/prerequisites/index.html))

### 🛠️ Build with `wasm-pack build`

```bash
wasm-pack build
```

### 🔬 Test in Headless Browsers with `wasm-pack test`

```bash
wasm-pack test --headless --firefox
wasm-pack test --headless --chrome
wasm-pack test --headless --safari
```

### 🎁 Publish new release to NPM

Before publish, build in release mode without default features :

```bash
wasm-pack build --release -- --no-default-features
```

You need npm token, if you don't have, get it with `npm login` command.

Then publish ( help) :

```bash
wasm-pack publish
```
